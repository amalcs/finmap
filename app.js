
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyPareser = require('body-parser');
var exphbs = require('express-handlebars');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var upload=require('express-fileupload');
const ipfsAPI = require('ipfs-api');
const fs = require('fs');
const qrcode = require('qrcode');
const qrimage=require('qr-image');
var privateKey = new Buffer('abcdef00', 'hex')
console.log(privateKey.toString('hex'))

var routes=require('./routes/index');
var users=require('./routes/users');

//init app
var app=express();

//View Engine
app.set('views',path.join(__dirname,'views'));
app.engine('handlebars',exphbs({defaultLayout:'layout'}));
app.set('view engine','handlebars');

app.use(upload());
//Bodyparser Middleware
app.use(bodyPareser.json());
app.use(bodyPareser.urlencoded({extended:false}));
app.use(cookieParser());

//set static folder
app.use(express.static(path.join(__dirname,'public')));

//Express session
app.use(session({
    secret:'secret',
    saveUninitialized:true,
    resave:false
}));

//passpor init
app.use(passport.initialize());
app.use(passport.session());




//Express validator
app.use(expressValidator({
    errorFormatter: function(param, msg, value) {
        var namespace = param.split('.')
        , root    = namespace.shift()
        , formParam = root;
  
      while(namespace.length) {
        formParam += '[' + namespace.shift() + ']';
      }
      return {
        param : formParam,
        msg   : msg,
        value : value
      };
    }
  }));

  //conncet flash
  app.use(flash());

  //global vars
  app.use(function(req,res,next){
      res.locals.success_msg=req.flash('success_mesg');
      res.locals.error_msg=req.flash('error_msg');
      res.locals.error=req.flash('error');
      next();
  });
  
  var sess;

  app.get('/',function(req,res){
    sess = req.session;
    //Session set when user Request our app via URL
    if(sess.email) {
    /*
    * This line check Session existence.
    * If it existed will do some action.
    */
        res.redirect('/admin');
    }
    else {
        res.render('login');
    }
    });

    //admin login
    app.get('/admin',function(req,res){
      sess = req.session;
    if(sess.email) {
    //  res.write('<h1>Hello '+sess.email+'</h1>');
    // res.end('<a href="/logout">Logout</a>');
    res.setHeader('Content-Type','text/html');
    res.redirect('/users/dashboard');

    } else {
        res.write('<h1>Please login first.</h1>');
        res.end('<a href="/">Login</a>');
    }
    });

    //voter login
    app.get('/voter',function(req,res){
      sess = req.session;
    if(sess.email) {
    //  res.write('<h1>Hello '+sess.email+'</h1>');
    // res.end('<a href="/logout">Logout</a>');
    res.setHeader('Content-Type','text/html');
    res.redirect('/users/user_cert');

    } else {
        res.write('<h1>Please login first.</h1>');
        res.end('<a href="/">Login</a>');
    }
    });
    app.post('/',function(req,res){
      sess = req.session;
    //In this we are assigning email to sess.email variable.
    //email comes from HTML page.
      sess.email=req.body.email;
      res.end('done');
    });

    app.get('/logout',function(req,res){
      req.session.destroy(function(err) {
        if(err) {
          console.log(err);
        } else {
          res.redirect('/');
        }
      });
    });

    app.get('/session',function(req,res){
     
      res.end(req.session.email);

    })

  //set routes
  app.use('/',routes);
  app.use('/users',users);


  //connecting to ipfs
  //Connceting to the ipfs network via infura gateway
const ipfs = ipfsAPI('ipfs.infura.io', '5001', {protocol: 'https'})

//Reading file from computer
//let testFile = fs.readFileSync("/home/akshay/FinMap/finmap/upload/myfile.pdf");
//Creating buffer for ipfs function to add file to the system
let testBuffer = new Buffer(testFile);

//Addfile router for adding file a local file to the IPFS network without any local node
app.get('/addfile', function(req, res) {
  let testFile = fs.readFileSync("/home/akshay/FinMap/finmap/upload/myfile.pdf");
  let testBuffer = new Buffer(testFile);

    ipfs.files.add(testBuffer, function (err, file) {
        if (err) {
          console.log(err);
          return;
        }
        res.render('admin/upload_page',{hash: file[0].hash, layout: 'admin_layout',title: 'Express', user: req.session.email});
        console.log(file[0].hash)
      }) 

})

//Getting the uploaded file via hash code.
app.get('/getfile', function(req, res) {
    
    //This hash is returned hash of addFile router.
    const validCID = 'HASH_CODE'

    ipfs.files.get(validCID, function (err, files) {
        files.forEach((file) => {
          console.log(file.path)
          console.log(file.content.toString('utf8'))
        })
      })

})

//Get the QR code
app.get("/qrcode", function(req, res){
  var data = req.query.data;
  var qr_svg = qrimage.image(data, { type: 'svg' });
  qr_svg.pipe(require('fs').createWriteStream('qr_code.svg'));
   
  var svg_string = qrimage.imageSync('I love QR!', { type: 'svg' });
  console.log("done");
  //alert("QRCode Successfully generated");
  // res.render('admin/upload_page')

})

//file upload
app.post("/file_upload",function(req,res){
  if(req.files){
    var file=req.files.filename,
      filename=file.name;
      file.mv("./upload/myfile.pdf",function(err){
        if(err){
          console.log(err);
          res.send("error occured");
        }
        else{
          res.redirect('/addfile');
        }
      })

  }
})


  //set port
  app.set('port',(process.env.PORT||3000));

  app.listen(app.get('port'),function(){
      console.log("server started on port"+app.get('port'));
  });

