var script = document.createElement('script');
script.src = '/js/newjquery-3.3.1.min.js';
document.head.appendChild(script);
var Web3 = require("web3");


if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
} 

web3.eth.defaultAccount = web3.eth.accounts[0];

var TestingContract = web3.eth.contract([
	{
		"constant": false,
		"inputs": [
			{
				"name": "FundId",
				"type": "uint256"
			},
			{
				"name": "borrower",
				"type": "address"
			}
		],
		"name": "acceptInvestment",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_pkey",
				"type": "address"
			},
			{
				"name": "CHash",
				"type": "string"
			}
		],
		"name": "addCertificate",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "investmentId",
				"type": "uint256"
			},
			{
				"name": "add",
				"type": "address"
			}
		],
		"name": "checkAcceptInvestment",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_fund_owner",
				"type": "address"
			},
			{
				"name": "amount",
				"type": "uint256"
			},
			{
				"name": "dueDate",
				"type": "string"
			}
		],
		"name": "newFund",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "invest_owner",
				"type": "address"
			},
			{
				"name": "FundId",
				"type": "uint256"
			},
			{
				"name": "rate",
				"type": "uint256"
			},
			{
				"name": "_amount",
				"type": "uint256"
			}
		],
		"name": "newInvestment",
		"outputs": [],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_add",
				"type": "address"
			},
			{
				"name": "_name",
				"type": "string"
			},
			{
				"name": "_pass",
				"type": "string"
			},
			{
				"name": "_credit",
				"type": "uint256"
			},
			{
				"name": "_balance",
				"type": "uint256"
			},
			{
				"name": "_verified",
				"type": "uint256"
			},
			{
				"name": "_asset",
				"type": "string"
			}
		],
		"name": "newUser",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"name": "fund_group",
		"outputs": [
			{
				"name": "fund_owner",
				"type": "address"
			},
			{
				"name": "state",
				"type": "uint8"
			},
			{
				"name": "amount",
				"type": "uint256"
			},
			{
				"name": "duedate",
				"type": "string"
			},
			{
				"name": "startdate",
				"type": "uint256"
			},
			{
				"name": "collected",
				"type": "uint256"
			},
			{
				"name": "invest_cnt",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "address"
			},
			{
				"name": "",
				"type": "uint256"
			}
		],
		"name": "fund_map",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "borrower",
				"type": "address"
			}
		],
		"name": "getActiveFundId",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "pkey",
				"type": "address"
			}
		],
		"name": "getCertificates",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "borrower",
				"type": "address"
			},
			{
				"name": "pos",
				"type": "uint256"
			}
		],
		"name": "getFundDetails",
		"outputs": [
			{
				"name": "",
				"type": "uint8"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_add",
				"type": "address"
			}
		],
		"name": "getUser",
		"outputs": [
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "fund_owner",
				"type": "address"
			}
		],
		"name": "hasActiveFundRequest",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"name": "investor",
		"outputs": [
			{
				"name": "add",
				"type": "address"
			},
			{
				"name": "req_id",
				"type": "uint256"
			},
			{
				"name": "state",
				"type": "uint8"
			},
			{
				"name": "interest",
				"type": "uint256"
			},
			{
				"name": "amount",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "address"
			},
			{
				"name": "",
				"type": "uint256"
			}
		],
		"name": "investor_map",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "owner",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "borrower",
				"type": "address"
			}
		],
		"name": "totalFundBy",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_fund_owner",
				"type": "address"
			}
		],
		"name": "totFundReq",
		"outputs": [
			{
				"name": "",
				"type": "uint256[]"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "totRequest",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_i",
				"type": "uint256"
			}
		],
		"name": "viewFund",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint8"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
]); 

var TestingC = TestingContract.at('0xbc873946151aa5a00295714e5b52505f6cbbf65d');
console.log(TestingC);



$("#register").click(function() {

	if($("#name").val()!="" && $("#credit").val()!="" && $("#verified").val()!="" && $("#password").val()!="" ){
    
	var asset="";
	console.log("Account details");
	var account=web3.personal.newAccount($("#pass").val());
	console.log(account);
    
	TestingC.newUser(account,$("#name").val(),$("#password").val(), $("#credit").val(),0,$("#verified").val(),asset,{from: web3.eth.defaultAccount, gas:3000000});
	alert("Voter Successfully added. \nKey : "+account);
	$("#name").val("");
	$("#credit").val("");
	$("#verified").val("");
}

});

//login page
function upload(){     
    if(document.getElementById("pass1").value==''||document.getElementById("id1").value=='')
        {
            alert('Please enter your Voter id');
            return false;
            
        }
    var userid 		= document.getElementById("id1").value;
    var password 	= document.getElementById("pass1").value;
	//console.log(password1);
	console.log(password);
    if((userid.localeCompare("admin")==0)&&(password.localeCompare("admin")==0)){
		
		$.post("http://localhost:3000/", {email:userid}, function(data){
			console.log('hello login');
			console.log(data);
			$("#head").text("hellll");
			if(data==='done')          
            {
				window.location.href="/admin";
				return true;            
			}

		});
		console.log('hello logiwwn');

		return false;
        
    }
TestingC.getUser(userid,function(error, result){
		console.log(result[1]);
		var vo="NO";
            var pass =result[1];
        //var str =result[4];
            //console.log(result[4]);
            //console.log("hello");
		//console.log(result[2]);//error
		if(result[6]==false)
				vo="YES";

		//var opr=web3.personal.unlockAccount(userid,pass);
		//console.log("login"+opr);
        if ((password.localeCompare(pass)==0)){
			$.post("http://localhost:3000/", {email:userid}, function(data){
				console.log('hello login');
				console.log(data);
				$("#head").text("hellll");
				if(data==='done')          
				{
					window.location.href="/admin";
					return true;            
				}
	
			});




                       }



    });
};
http://remix.ethereum.org/#optimize=false&version=builtin
//view candidate
function get_voter(){
	if(document.getElementById("id1").value==""){Start
		alert("Please enter valid candidate ID");Start
		return false;
	}
	var userid=$('#id1').val();
	var password 	= document.getElementById("pass1").value;
	TestingC.getUser(userid,function(error, result){
		console.log(error);
		var vo="YES";
            var pass =result[2];
        var str =result[4];
            console.log(result[4]);
            //console.log("hello");
		console.log(result[2]);//error
		if(result[6]==false)
				vo="NO";        
        if ((password.localeCompare(pass)==0)){
						   console.log("true");
						   $("#my_div").show();
						   $("#name").text(result[0]);
						   $("#email").text(result[1]);
						   $("#count").text(result[3]);
						   $("#gender").text(result[4]);
						   $("#voted").text(vo);
                       }
    });

}
//Confirm Certificate
function confirm_certificate(){
	if(document.getElementById("pkey").value=="" || document.getElementById('url').value==""){
		alert("Please enter candidate details");
		return false;
	}
	var pubkey=document.getElementById("pkey").value;
	var chash=document.getElementById("url").text;
	console.log(chash);
	TestingC.addCertificate(pubkey,chash,{from: web3.eth.defaultAccount, gas:3000000});
	alert("Certificate successfully inserted\nValue : "+chash);
	document.getElementById("con_cer").disabled = true;

}



var email="";
//load the voting panel
// $(document).ready(function() {

// 	// $.get("http://localhost:3000/session", function(data){
		
// 	// 	console.log("inner "+data);
// 	// 	email=data;
// 	// 	console.log(email+"inner");
// 	// 	});
// 	// console.log("connection"+email);
// 	var u_id=document.getElementById('user_id').value;
// 	console.log(window.location.pathname=="/users/user_cert"); 
// 	var html1="";
// 	var but="";
// 	var flag=0;
// 	if((window.location.pathname=="/users/voting") || (window.location.pathname=="/users/user_cert")){
// 	TestingC.getCertificates(u_id,function(error, result){
// 		console.log(result);
// 		//html1="</br></br><table class='table'><tr><th> <b>Student Publickey</b></th><td>"+u_id+"</td></tr><tr><th><b>Certificate Hash</b></th><td><a href='https://ipfs.io/ipfs/"+result+">"+result+"</a></td></tr> </table>";
// 		$("#pubkey").html("<h4><p>"+u_id+"</p></h4>");
// 		var link = document.getElementById("url");
// 		link.innerHTML = result;
// 		link.setAttribute('href', "https://ipfs.io/ipfs/"+result);


		
// 	});

//  }

// })


//Generate Certificate
function generate_certificate(){
	window.location.href = "/addfile";
}

//collect vote data
function voteCandidate(){			//$('#vote_list').change(function(){
	var radioValue = $("input[name='candidate']:checked").val();
	console.log(radioValue);
	var id=document.getElementById('voter_id').value;
	TestingC.getUser(id,function(error, result){
		console.log(result[6].toString()==0);
		if(result[6].toString()==0){
			TestingC.updateUser(id,function(error,result){
			TestingC.voteForCandidate(radioValue,function(error, result){
            var cnt =result.toString();
	});
})
}
});
}




// fund request
$("#request_fund").click(function() {
	if(document.getElementById("id").value=="" || document.getElementById('amount').value==""){
		alert("Please enter candidate details");
		return false;
	}
	var userid=document.getElementById("id").value;
	var amount=document.getElementById("amount").value;
	var d_date=document.getElementById("d_date").value;

	TestingC.newFund(userid,amount,d_date,{from: web3.eth.defaultAccount, gas:3000000});
	alert("successfully Requested");
	$("#id").val("");
	$("#amount").val("");
	$("#d_date").val("");
});

//View All Fund Requests
$("#all_request_fund").click(function() {
	var u_id=document.getElementById('id').value;
	var html1="";
	var but="";
	var flag=0;
	console.log("I am here");
	TestingC.totFundReq(u_id,function(error, result_count){
		console.log(result_count);
		console.log("first"+result_count[0]);
		$("#my_div").show();
		var j=1;
		for(i=0;i<result_count.length;i++){
			k=result_count[i];
			console.log("K "+k);
			TestingC.viewFund(result_count[i],function(error, result){
				console.log("re count"+k);	//due date
				console.log("id"+result[2].toString());	//id
				var id=result[2].toString();
				var name=result[0];
				date = new Date(result[2] * 1000);
				c_day=date.getDate();
				c_year=  date.getFullYear();
				c_month=date.getMonth()+1;
				c_date=c_year+"-"+c_month+"-"+c_day;
				console.log("date "+c_month);
				console.log(result[2].toString());
				html1+="<tr><td>"+(j++)+"</td><td>"+result[0]+"</td><td>"+result[1]+"</td><td>"+c_date+"</td><td>"+result[3]+"</td><td>"+result[4]+"</td><td>"+result[5]+"</td>";
				if(result[5]==0){
				html1+="<td><button style='height:30px;width:100px; float:right' type='submit' class='btn-success' id='"+k+"' onclick='accept_fund(id)'> Accept </button></td>"
				}
				html1+="</tr>";
				$("#data_body").html(html1);
			
			});
		}
		
	});

});

//View All Fund Requests for investment
$("#invest_fund").click(function() {
	var html1="";
		var j=1;
		var i=0;
		amount=0;
		TestingC.totRequest(function(error, result){
			console.log(result);
		while(i<result.toString()){
		TestingC.viewFund(i,function(error, result){

				//console.log(result[1]);	//due date
				//console.log("id"+result[2].toString());	//id
				var id=result[2].toString();
				var name=result[0];
				date = new Date(result[2] * 1000);
				c_day=date.getDate();
				c_year=  date.getFullYear();
				c_month=date.getMonth()+1;
				c_date=c_year+"-"+c_month+"-"+c_day;
				console.log("date "+result[0]);
				console.log(result[2].toString());
				if(result[5]==0){
				html1+="<tr><td>"+(j++)+"</td><td>"+result[0]+"</td><td>"+result[1]+"</td><td>"+c_date+"</td><td>"+result[3]+"</td><td>"+result[4]+"</td><td>"+result[5]+"</td>";
				html1+="<td><button style='height:30px;width:100px; float:right' type='submit' class='btn-success' id='"+(j-2)+"' onclick='new_proposal(id)'> Proposal </button>";
				html1+"</td></tr>";
				}
				$("#data_body").html(html1);
			
			});
			i++;
			console.log("amt "+amount);
		}
	});

		
});

//new proposal
function new_proposal(id){			
	console.log("I am here"+id);
	amount = prompt("Please mention the amount you would like to invest")
	if(interest = prompt("The profit share rate you would like to bid?")){
	var u_id=document.getElementById('id').value;
	console.log(u_id);
	TestingC.newInvestment(u_id,id,interest,amount,{from: web3.eth.defaultAccount, gas:3000000});
	alert("Proposal added Successfully");
	}
	else{
		alert("Not Successful");

	}
}

//Accept fund
function accept_fund(id){			
	console.log("I am here "+id);
	var u_id=document.getElementById('id').value;
	console.log(u_id);
	TestingC.acceptInvestment(id,u_id,{from: web3.eth.defaultAccount, gas:3000000});
	alert("Proposal accepted Successfully");
	
}

//View Profile information
$("#check_profile").click(function() {
	var html1="";
		$("#my_div").show();
		var u_id=document.getElementById('id').value;
		TestingC.getUser(u_id,function(error, result){
			html1+="<tr><td>"+result[0]+"</td><td>"+result[2]+"</td><td>"+result[3]+"</td><td>"+result[4]+"</td><td><a id='url' name='url' href='https://ipfs.io/ipfs/"+result[5]+"'>"+result[5]+"</a></td></tr>";
			$("#profile").html(html1);

	});

		
});




