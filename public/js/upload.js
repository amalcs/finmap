const ipfsAPI = require('ipfs-api');
const multer = require('multer');
const path = require('path');
const fs = require('fs');

var express = require('express');
var router = express.Router();
const MAX_SIZE = 52428800; 


/*  upload GET endpoint. */
router.get('/addfile', function(req, res, next) {
    res.send('Upload endpoint!');
  });
 
/*  upload POST endpoint */
router.post('/addfile', upload.single('file'), (req, res) => {
    if (!req.file) {
      return res.status(422).json({
        error: 'File needs to be provided.',
      });
    }
  
    const mime = req.file.mimetype;
    if (mime.split('/')[0] !== 'image') {
      fs.unlink(req.file.path);
  
      return res.status(422).json({
        error: 'File needs to be an image.',
      });
    }
  
    const fileSize = req.file.size;
    if (fileSize > MAX_SIZE) {
      fs.unlink(req.file.path);
  
      return res.status(422).json({
        error: `Image needs to be smaller than ${MAX_SIZE} bytes.`,
      });
    }
  
    const data = fs.readFileSync(req.file.path);
    return ipfs.add(data, (err, files) => {
      fs.unlink(req.file.path);
      if (files) {
        return res.json({
          hash: files[0].hash,
        });
      }
  
      return res.status(500).json({
        error: err,
      });
    });
  });