var express=require('express');
var router=express.Router();
var session = require('express-session');


//get hame page
router.get('/register',function(req,res){
    res.render('register');
});

//login
router.get('/login',function(req,res){
    res.render('login');
});

router.get('/dashboard',function(req,res){
    res.render('admin/dashboard',{layout: 'admin_layout', title: 'Express', user: req.session.email });
});

router.get('/add_candidate',function(req,res){
    console.log("hello"+req.session.email);

    res.render('admin/add_candidate',{layout: 'admin_layout'});
});

//Add new User
router.get('/add_user',function(req,res){
    res.render('admin/add_user',{layout: 'admin_layout',title: 'Express', user: req.session.email});
});

//User profile
router.get('/profile',function(req,res){
    res.render('admin/profile',{layout: 'admin_layout',title: 'Express', user: req.session.email});
});

router.get('/check_vote',function(req,res){
    res.render('admin/check_vote',{layout: 'admin_layout'});
});

router.get('/request_fund',function(req,res){
    res.render('admin/request_fund',{layout: 'admin_layout',title: 'Express', user: req.session.email});
});
//view fund request
router.get('/view_fund_request',function(req,res){
    res.render('admin/view_fund_request',{layout: 'admin_layout',title: 'Express', user: req.session.email});
});

router.get('/view_voter',function(req,res){
    res.render('admin/view_voter',{layout: 'admin_layout'});
});

router.get('/verify_cert',function(req,res){
    res.render('admin/cert_validation',{layout: 'admin_layout',title: 'Express', user: req.session.email});
});

//voter voting machine
router.get('/user_cert',function(req,res){
    res.render('voter/user_home',{title: 'Express', user: req.session.email});
});

//add Certificate
router.get('/add_cert',function(req,res){
    res.render('admin/generate_cert',{layout: 'admin_layout',title: 'Express', user: req.session.email});
});

//Invest Fund
router.get('/invest_fund',function(req,res){
    res.render('admin/invest_fund',{layout: 'admin_layout',title: 'Express', user: req.session.email});
});

router.get('/session',function(req,res){
return req.session.email;
});

//upload files
router.get('/getupload_page',function(req,res){
    res.render('admin/upload_page',{layout: 'admin_layout',title: 'Express', user: req.session.email});
})




module.exports=router;